// Copied from ERC721 test

import assertRevert from 'openzeppelin-solidity/test/helpers/assertRevert';
const BigNumber = web3.BigNumber;
const Ownership = artifacts.require('OwnershipMock.sol');

require('chai')
  .use(require('chai-as-promised'))
  .use(require('chai-bignumber')(BigNumber))
  .should();

contract('Ownership', accounts => {
  let token = null;
  const _firstTokenId = 101;
  const _secondTokenId = 102;
  const _unknownTokenId = 103;
  const _creator = accounts[0];
  const ZERO_ADDRESS = '0x0000000000000000000000000000000000000000';

  beforeEach(async function () {
    token = await Ownership.new("name", "sym", { from: _creator });
  });

  let tokenStart = 1;

  describe('minting items', function () {
    describe('before minting an item', function () {
      it('has correct next token id', async function () {
        const nextTokenId = await token.getNextTokenId();
        assert.equal(nextTokenId.toNumber(), tokenStart, "wrong token id");
      });
    });

    describe('after minting an item', function () {
      it('has correct next token id', async function () {
        await token.mintItemTo(accounts[1], 123);
        await token.mintItemTo(accounts[1], 456);

        const nextTokenId = await token.getNextTokenId();
        assert.equal(nextTokenId.toNumber(), tokenStart+2, "wrong token id");

        const tokens = await token.balanceOf(accounts[1]);
        tokens.should.be.bignumber.equal(2);

        const result = await token.itemsOf(accounts[1]);
        result[0][0].should.be.bignumber.equal(123);
        result[0][1].should.be.bignumber.equal(456);
      });
    });
  });
});
