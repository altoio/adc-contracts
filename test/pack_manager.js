var PackManager = artifacts.require('../contracts/PackManager.sol');
var PackItems   = artifacts.require('../contracts/PackItems.sol');
var ItemManager = artifacts.require('../contracts/ItemManager.sol');
var MainStorage = artifacts.require('../contracts/MainStorage.sol');
var Ownership = artifacts.require('../contracts/Ownership.sol');

contract('PackManager & PackItems', function(accounts) {

  let packManager;
  let packItems;
  let itemManager;
  let mainStorage;
  let ownership;

  let packId = 1;
  let size = 10;
  let price = web3.toWei(2, "finney");
  let stock = 10;

  let size2 = 15;
  let price2 = web3.toWei(3, "finney");
  let stock2 = 20;

  let MAX_UINT256 = (2 ** 256) - 1;
  let SIZE = (2**128) - 1; // size of a group (ItemDef, TokenDef)

  let MAX_PACK_DEF = MAX_UINT256;
  let MIN_PACK_DEF = MAX_PACK_DEF - SIZE;

  before(async function()  {
    itemManager = await ItemManager.deployed();
    mainStorage = await MainStorage.deployed();
    packManager = await PackManager.new(mainStorage.address);
    packItems = await PackItems.new(mainStorage.address);
    ownership = await Ownership.deployed();
    ownership.setAccess(packManager.address, true);

    mainStorage.setContract("packmanager", packManager.address);
    mainStorage.setContract("packitems", packItems.address);

    await itemManager.createItems(10);
    await itemManager.createItems(10);
    await itemManager.createItems(10);
    await itemManager.createItems(10);
    await itemManager.createItems(10);
  });


  describe("creating packs", function() {
    it("should create and update a pack", async function()  {
      let result = await packManager.getPackCount();
      assert.equal(result.toNumber(), 0, "pack count should be 0");

      await packManager.createPack(size, price, stock);

      result = await packManager.getPackCount();
      assert.equal(result.toNumber(), 1, "pack count did not increase");

      result = await packManager.getPack(packId);
      assert.equal(result[0].toNumber(),size,"size did not match");
      assert.equal(result[2].toNumber(),stock,"stock did not match");
      assert.equal(result[3],false,"isReleased should be false");
      assert.equal(result[4].toNumber(),price,"price did not match");

      await packManager.updatePack(packId, size2, price2, stock2);

      result = await packManager.getPack(packId);
      //console.log(result);
      assert.equal(result[0].toNumber(),size2,"size did not match");
      assert.equal(result[2].toNumber(),stock2,"stock did not match");
      assert.equal(result[3],false,"isReleased should be false");
      assert.equal(result[4].toNumber(),price2,"price did not match");

      // 0,1,2,3 tokens are for the initial items defined above
      result = await ownership.tokenOfOwnerByIndex(accounts[0],4);
      //console.log(result[4].toString(16));
      assert.equal(result.toNumber(), MIN_PACK_DEF, "token id is wrong");

      result = await ownership.packDefsOf(accounts[0]);
      assert.equal(result[0][0].toNumber(), 1, "pack id is wrong");
    });

    it("should create multiple packs", async function()  {
      let sizes = [1,2,3,4,5,6,7,8,9,10,11,12,13,14];

      let result = await packManager.createPacks(sizes, sizes, sizes);
      result = await packManager.getPackCount();
      assert.equal(
        result.toNumber(), 
        sizes.length + 1, 
        "pack count did not increase"
      );

      for (let i=0; i<sizes.length; i++) {
        result = await packManager.getPack(i+2);
        assert.equal(result[0].toNumber(),sizes[i],"size did not match");
        assert.equal(result[2].toNumber(),sizes[i],"stocks did not match");
      }
    });
  });

  describe("setting pack items", function() {
    it("should set a pack item", async function()  {
      let weight = 100;
      await packItems.setPackItem(packId, 1, weight);

      let result = await packItems.getPackItem(packId, 1);
      assert.equal(result[0].toNumber(), 1, "item id did not match");
      assert.equal(result[1].toNumber(), weight, "weight did not match");
      assert.equal(result[2], true, "active is false");

      await packItems.setPackItem(packId, 2, weight);
    });

    it("should delete an item from the pack", async function()  {
      let itemId = 1;
      await packItems.removePackItem(packId, itemId);
      let result = await packItems.getPackItem(packId, itemId);
      assert.equal(result[2], false, "item is still active in the pack");
    });

    it("should bring back deleted item", async function()  {
      let itemId = 1;
      await packItems.setPackItem(packId, itemId, 100);
      let result = await packItems.getPackItem(packId, itemId);
      assert.equal(result[2], true, "item is still not active in the pack");
    });

    it("should create multiple items", async function()  {
      let items = [
            1, 2, 3, 4, 5, 6, 7, 8, 9,
        10,11,12,13,14,15,16,17,18,19,
        20,21,22,23,24,25,26,27,28,29,
        30,31,32,33,34,35,36,37,38,39,
        40,41,42,43,44,45
      ];
      let weights = items;
      let packIdToChange = 2;

      let result = await packItems.setPackItems(packIdToChange, items, weights);
      //console.log(result);
      result = await packManager.getPack(packIdToChange);
      assert.equal(result[5].toNumber(), items.length, "item count is wrong");
    });
  });

  describe("modify pack properties", function() {
    it("should change the pack price", async function()  {
      price2 = web3.toWei(4, "finney");
      await packManager.setSellingPrice(packId, price2);
      let result = await packManager.getPack(packId);
      assert.equal(result[4], price2, "size did not match");
    });

    it("should change the pack size", async function()  {
      size2 = 21;
      await packManager.setPackSize(packId, size2);
      let result = await packManager.getPack(packId);
      assert.equal(result[0], size2, "size did not match");
    });

    it("should change the pack stock", async function()  {
      stock2 = 22;
      await packManager.setPackStock(packId, stock2);
      let result = await packManager.getPack(packId);
      assert.equal(result[2], stock2, "stock did not match");
    });

    it("should return that pack is not released", async function()  {
      let result = await packManager.isPackReleased(packId);
      assert.equal(result, false, "is already released");
    });

    it("should release the pack", async function()  {
      await packManager.releasePack(packId);
      let result = await packManager.isPackReleased(packId);
      assert.equal(result, true, "isReleased is still false");
    });

    it("should return pack info", async function()  {
      let result = await packManager.getPack(packId);

      assert.equal(result[0].toNumber(),size2,"size did not match");
      assert.equal(result[2].toNumber(),stock2,"stock did not match");
      assert.equal(result[3],true,"isReleased should be false");
      assert.equal(result[4].toNumber(),price2,"price did not match");
    });

    it("should return the item count in a pack", async function()  {
      let result = await packManager.getPack(packId);
      assert.equal(result[5].toNumber(), 2, "pack does not contain an item");
    });

    it("should return that pack is released", async function()  {
      let result = await packManager.isPackReleased(packId);
      assert.equal(result, true, "is not released yet");
    });

    it("should return if pack is sold out", async function()  {
      let result = await packManager.isPackSoldOut(packId);
      assert.equal(result, false, "is already sold out");
    });

    it("should check if a pack exists", async function()  {
      let result = await packManager.packExists(packId);
      assert.equal(result, true, "pack doesn't exist");

      result = await packManager.packExists(99);
      assert.equal(result, false, "pack exists");
    });
  });

  describe("pack items bitmask", function() {
    it("should set pack item id correctly", async function() {
      let itemId = 123456;
      let packed = await packItems.setPackItemId(0, itemId);
      let result = await packItems.getPackItemId(packed);

      assert.equal(result.toNumber(), itemId, "item ids are not equal");
    });

    it("should set pack item weight correctly", async function() {
      let weight = 15;
      let packed = await packItems.setPackItemWt(0, weight);
      let result = await packItems.getPackItemWt(packed);

      assert.equal(result.toNumber(), weight, "weights are not equal");
    });

    it("should set pack item active correctly", async function() {
      let packed = await packItems.setPackItemAct(0, true);
      let result = await packItems.getPackItemAct(packed);

      assert.equal(result, true, "active are not equal");

      packed = await packItems.setPackItemAct(0, false);
      result = await packItems.getPackItemAct(packed);

      assert.equal(result, false, "active are not equal");
    });
  });
});
