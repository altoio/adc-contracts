import assertRevert from 'openzeppelin-solidity/test/helpers/assertRevert';

var MainStorage = artifacts.require('../contracts/MainStorage.sol');
var ItemManager = artifacts.require('../contracts/ItemManager.sol');

contract('MainStorage', function(accounts) {
  let mainStorage;
  let itemManager;
  let keyTemp1 = "keyTemp1";
  let keyTemp2 = "keyTemp2";
  let noAccess = accounts[1];

  let valUint = 100;
  let valInt = 101;
  let valString = "hello";
  let valAddress = accounts[2];
  let valBool = true;

  before(async function() {
    mainStorage = await MainStorage.deployed();
    itemManager = await ItemManager.deployed();
  });

  describe("access checks", function() {
    it("should be able to access from owner", async function() {
      await mainStorage.setUint(keyTemp2, 1);
    });

    it("should not be able to access", async function() {
      await assertRevert(
        mainStorage.setUint(keyTemp2, 1, {from:noAccess})
      );
    });

    it("should be able to set a new owner", async function() {
      await mainStorage.setOwner(accounts[2]);
      await assertRevert(mainStorage.setUint(keyTemp2, 1));
      await mainStorage.setUint(keyTemp2, 1, {from:accounts[2]});
      //set it back
      await mainStorage.setOwner(accounts[0], {from:accounts[2]});
    });
  });

  describe("set contract", function() {
    it("should set contract address", async function() {
      await mainStorage.setContract("itemmanager", itemManager.address);

      let addr = await mainStorage.getContractByAddress(itemManager.address);
      assert.equal(addr, itemManager.address, "addresses don't match");

      addr = await mainStorage.getContractByName("itemmanager");
      assert.equal(addr, itemManager.address, "addresses don't match");
    });

    it("should delete contract address", async function() {
      await mainStorage.deleteContract("itemmanager", itemManager.address);

      let addr = await mainStorage.getContractByAddress(itemManager.address);
      assert.notEqual(addr, itemManager.address, "addresses still match");

      addr = await mainStorage.getContractByName("itemmanager");
      assert.notEqual(addr, itemManager.address, "addresses still match");
    });
  });

  describe("uint", function() {
    it("should set a uint", async function() {
      let result = await mainStorage.getUint(keyTemp1)
      assert.equal(result.toNumber(), 0, "initial value is not 0");

      await mainStorage.setUint(keyTemp1, valUint);

      result = await mainStorage.getUint(keyTemp1)
      assert.equal(result.toNumber(), valUint, "value is not set");
    });

    it("should delete uint", async function() {
      await mainStorage.deleteUint(keyTemp1);

      let result = await mainStorage.getUint(keyTemp1)
      assert.equal(result.toNumber(), 0, "initial value is not 0");
    });
  });

  describe("string", function() {

    it("should set a string", async function() {
      let result = await mainStorage.getString(keyTemp1)
      assert.equal(result.toString(), "", "initial value is not empty");

      await mainStorage.setString(keyTemp1, valString);

      result = await mainStorage.getString(keyTemp1)
      assert.equal(result.toString(), valString, "value is not set");
    });

    it("should delete string", async function() {
      await mainStorage.deleteString(keyTemp1);

      let result = await mainStorage.getString(keyTemp1)
      assert.equal(result.toString(), "", "initial value is not empty");
    });

  });

  describe("address", function() {

    it("should set a address", async function() {
      let result = await mainStorage.getAddress(keyTemp1)
      assert.equal(result, 0, "initial value is not 0");

      await mainStorage.setAddress(keyTemp1, valAddress);

      result = await mainStorage.getAddress(keyTemp1)
      assert.equal(result.toString(), valAddress.toString(), "value is not set");
    });

    it("should delete address", async function() {
      await mainStorage.deleteAddress(keyTemp1);

      let result = await mainStorage.getAddress(keyTemp1)
      assert.equal(result, 0, "initial value is not 0");
    });

  });

  describe("bytes32", function() {

    it("should set a bytes32", async function() {
      let result = await mainStorage.getBytes32(keyTemp1)
      assert.equal(web3.toUtf8(result), "", "initial value is not empty");

      await mainStorage.setString(keyTemp1, valString);

      result = await mainStorage.getString(keyTemp1)
      assert.equal(result, "hello", "value is not set");
    });

    it("should delete bytes32", async function() {
      await mainStorage.deleteString(keyTemp1);

      let result = await mainStorage.getBytes32(keyTemp1)
      assert.equal(web3.toUtf8(result), "", "initial value is not empty");
    });

  });

  describe("bool", function() {

    it("should set a bool", async function() {
      let result = await mainStorage.getBool(keyTemp1)
      assert.equal(result, 0, "initial value is not false");

      await mainStorage.setBool(keyTemp1, valBool);

      result = await mainStorage.getBool(keyTemp1)
      assert.equal(result, valBool, "value is not set");
    });

    it("should delete bool", async function() {
      await mainStorage.deleteBool(keyTemp1);

      let result = await mainStorage.getBool(keyTemp1)
      assert.equal(result, 0, "initial value is not false");
    });

  });

  describe("int", function() {

    it("should set a int", async function() {
      let result = await mainStorage.getInt(keyTemp1)
      assert.equal(result.toNumber(), 0, "initial value is not 0");

      await mainStorage.setInt(keyTemp1, valInt);

      result = await mainStorage.getInt(keyTemp1)
      assert.equal(result.toNumber(), valInt, "value is not set");
    });

    it("should delete int", async function() {
      await mainStorage.deleteInt(keyTemp1);

      let result = await mainStorage.getInt(keyTemp1)
      assert.equal(result.toNumber(), 0, "initial value is not 0");
    });

  });
});
