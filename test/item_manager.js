import assertRevert from 'openzeppelin-solidity/test/helpers/assertRevert';

var ItemManager = artifacts.require('../contracts/ItemManager.sol');
var Ownership = artifacts.require('../contracts/Ownership.sol');
var ItemStore   = artifacts.require('../contracts/ItemStore.sol');
var PackManager = artifacts.require('../contracts/PackManager.sol');
var PackItems   = artifacts.require('../contracts/PackItems.sol');

contract('ItemManager', function(accounts) {

  let newCEO = accounts[1];
  let instance;
  let ownership;
  let itemStore;
  let packManager;
  let packItems;
  let itemId = 1;
  let itemIdInvalid = 12;
  let price = web3.toWei(2, "ether");

  let MAX_UINT256 = (2 ** 256) - 1;
  let SIZE = (2**128) - 1; // size of a group (ItemDef, TokenDef)

  let MAX_PACK_DEF = MAX_UINT256;
  let MIN_PACK_DEF = MAX_PACK_DEF - SIZE;

  let MAX_ITEM_DEF = MIN_PACK_DEF - 1;
  let MIN_ITEM_DEF = MAX_ITEM_DEF - SIZE;

  before(async function()  {
    instance = await ItemManager.deployed();
    ownership = await Ownership.deployed();
    itemStore = await ItemStore.deployed();
    packManager = await PackManager.deployed();
    packItems = await PackItems.deployed();
  });

  describe("access control", function() {
    describe("ceo access", function () {
      it("should be CEO", async function()  {
        let result = await instance.ceoAddress.call();
        assert.equal(result, accounts[0], "CEO address is wrong");
      });

      it("should change the CEO address", async function()  {
        await instance.setCEO(newCEO);
        let result = await instance.ceoAddress.call();
        assert.equal(result, newCEO, "CEO address is wrong");
      });

      it("reverts if no access", async function() {
        await assertRevert(instance.createItem({from:accounts[4]}));
      });
    });

    describe("pausing", function () {
      it("should pause the contract", async function()  {
        await instance.pause({from:newCEO});
        let result = await instance.paused.call();
        assert.equal(result, true, "is not paused");
      });

      it("reverts if create item when paused", async function() {
        await assertRevert(
          instance.createItem({from:newCEO})
        );
      });

      it("should unpause the contract", async function()  {
        await instance.unpause({from:newCEO});
        let result = await instance.paused.call();
        assert.equal(result, false, "is paused");
      });
    });

    describe("payable", function () {
      it("should be able to send eth to contract", async function()  {
        await web3.eth.sendTransaction({
          from:accounts[0], to:instance.address, value:price});

        let balContract = await web3.eth.getBalance(instance.address);
        assert.equal(balContract.toNumber(), price, "contract balance is not correct");
      });

      it("should withdraw balance of the contract to ceo address", async function()  {
        let balCEOPrev = await web3.eth.getBalance(newCEO);
        await instance.withdrawBalance({from:newCEO});
        let balCEONew = await web3.eth.getBalance(newCEO);
        assert.isTrue(balCEONew.toNumber() > balCEOPrev.toNumber());
      });

    });

    describe("give access", function() {
      it("should add access to an address", async function()  {
        await instance.setAccess(accounts[3], true, {from:newCEO});
        let hasAccess = await instance.hasAccess(accounts[3]);
        assert.isTrue(hasAccess, "access was not granted");
      });

      it("should remove access to an address", async function()  {
        await instance.setAccess(accounts[3], false, {from:newCEO});
        let hasAccess = await instance.hasAccess(accounts[3]);
        assert.isFalse(hasAccess, "access was still granted");
      });

      it("should have no access if account is not added", async function()  {
        let hasAccess = await instance.hasAccess(accounts[4]);
        assert.isFalse(hasAccess, "access was granted");
      });
    });
  });

  describe("item storage", function() {

    describe("create items", function() {
      it("should create 1 item", async function()  {
        await instance.createItem({from:newCEO});

        let itemCount = await instance.getItemCount();
        assert.equal(itemCount.toNumber(), 1, "item count != 1");

        let token = await ownership.tokenOfOwnerByIndex(newCEO, 0);
        //console.log(tokens[0].toString(16));
        assert.equal(token.toNumber(), MIN_ITEM_DEF, "token is wrong");

        let result = await ownership.itemDefsOf(newCEO);
        assert.equal(result[0][0].toNumber(), 1, "item id != 1");
      });

      it("should create 10 items", async function()  {
        let account = accounts[3];
        await instance.setAccess(account,true, {from:newCEO});
        await instance.createItems(10, {from:account});

        let itemCount = await instance.getItemCount();
        assert.equal(itemCount.toNumber(), 11, "item count != 11");

        let result = await ownership.itemDefsOf(account);
        assert.equal(result[0][0].toNumber(), 2, "item id != 1");
        assert.equal(result[0][1].toNumber(), 3, "item id != 1");
        assert.equal(result[0][9].toNumber(), 11, "item id != 1");
      });
    });

    describe("item checks", function() {
      it("should check if item exists", async function()  {
        let result = await instance.itemExists(itemId);
        assert.isTrue(result, "item doesn't exist");
      });

      it("should check if item does not exist", async function()  {
        let result = await instance.itemExists(999);
        assert.isFalse(result, "item exists");
      });
    });

    describe("item dna", function() {
      it("should get 0 as undefined dna", async function() {
        let dnaOther = await instance.getDNA(1, accounts[0]);
        assert.equal(dnaOther.toNumber(), 0, "dna default is not 0");
      });

      it("should set the item dna", async function() {
        let dnaOwner = accounts[3];
        await instance.setDNA(1, 0xffffff, {from:dnaOwner});

        let dna = await instance.getDNA(1, dnaOwner);
        assert.equal(dna.toNumber(), 0xffffff, "dna is not equal");
      });
    });

    describe("item mutable dna", function() {
      let itemId = 2;
      let game = accounts[4];
      let player = accounts[5];
      let token;

      it("should get 0 as undefined mutable dna", async function() {
        let dnaResult = await instance.getMutableDNA(itemId, game, 1, {from:player});
        assert.equal(dnaResult.toNumber(), 0, "dna default is not 0");
      });

      it("should be able to set a mutable dna", async function() {
        let price = web3.toWei(1, "finney");
        await packManager.createPack(1, price, 10);
        await packItems.setPackItem(1, itemId, 1);
        await packManager.releasePack(1);
        await itemStore.buyPack(1, {from:player, value:price});

        token = await ownership.tokenOfOwnerByIndex(player,0);
        let dna = 1234;
        await instance.setMutableDNA(
          itemId,
          game,
          token,
          dna,
          {from:player}
        );

        let dnaResult = await instance.getMutableDNA(
          itemId,
          game,
          token,
          {from:player}
        );
        assert.equal(dnaResult.toNumber(), dna, "dna does not match");
      });

      it("should revert if not the player", async function() {
        await assertRevert(
          instance.setMutableDNA(itemId, game, token, 4567)
        );
      });
    });
  });

});
