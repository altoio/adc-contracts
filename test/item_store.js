import assertRevert from 'openzeppelin-solidity/test/helpers/assertRevert';

var ItemStore   = artifacts.require('../contracts/ItemStore.sol');
var Ownership   = artifacts.require('../contracts/Ownership.sol');
var ItemManager = artifacts.require('../contracts/ItemManager.sol');
var PackManager = artifacts.require('../contracts/PackManager.sol');
var PackItems   = artifacts.require('../contracts/PackItems.sol');
var MainStorage = artifacts.require('../contracts/MainStorage.sol');

contract('ItemStore', function(accounts) {

  let instance;
  let itemManager;
  let ownership;
  let packManager;
  let packItems;
  let mainStorage;

  let packId = 1;
  let size = 15;
  let itemsCount = 40;
  let price = web3.toWei(2, "finney");
  let stock = 10;

  //--------------------------------------------------------------------------
  before(async function()  {
    itemManager = await ItemManager.deployed();
    ownership   = await Ownership.deployed();
    packManager = await PackManager.deployed();
    packItems   = await PackItems.deployed();
    mainStorage = await MainStorage.deployed();

    instance = await ItemStore.new(mainStorage.address);

    mainStorage.setContract("itemstore", instance.address);

    itemManager.setAccess(instance.address, true);
    packManager.setAccess(instance.address, true);
    packItems.setAccess(instance.address, true);
    ownership.setAccess(instance.address, true);

    await packManager.createPack(size, price, stock);

    let batchSize = 10;
    let counter = 0;
    for (let i=0; i<itemsCount; ) {
      let diff = itemsCount - counter;
      if (diff > batchSize)
        diff = batchSize;
      await itemManager.createItems(batchSize);
      i += diff;
    }

    let items = [];
    let weights = [];
    for (let i=0; i<itemsCount; i++) {
      items[i] = i+1;
      weights[i] = 1;
    }

    await packItems.setPackItems(packId, items, weights);
    await packManager.releasePack(packId);
  });

  //--------------------------------------------------------------------------
  it("should buy a pack with 1 item type", async function()  {
    let pack2 = 2;

    await packManager.createPack(2, price, stock);

    await packItems.setPackItem(pack2, 1, 255);
    await packItems.setPackItem(pack2, 2, 1);
    await packManager.releasePack(pack2);

    let balContractPrev = await web3.eth.getBalance(instance.address);

    let transaction = await instance.buyPack(pack2, {value:price});

    let balContractNew = await web3.eth.getBalance(instance.address);
    assert.isTrue(balContractNew.toNumber() - balContractPrev.toNumber() == price,
      "contract didn't get expected eth");

    let result = await ownership.itemsOf(accounts[0]);

    assert.equal(result[0][0].toNumber(), 1, "Didn't get the correct item");
    assert.equal(result[0][1].toNumber(), 1, "Didn't get the correct item");

    let pack = await packManager.getPack(pack2);
    assert.equal(pack[1].toNumber(),1,"minted did not match");
    assert.equal(pack[2].toNumber(),stock,"stock did not match");

    let mintedCount = await instance.getMintedCount(1);
    assert.equal(mintedCount.toNumber(), 2, "minted count did not increase");
  });

  //--------------------------------------------------------------------------
  it("should get items minted count", async function()  {
    let counts = await instance.getItemsMintedCount();
    assert.equal(counts[0], 0, "[0] minted count is wrong");
    assert.equal(counts[1], 2, "[1] minted count is wrong");
  });

  //--------------------------------------------------------------------------
  it("should buy a pack", async function()  {
    let acc = accounts[1];

    let transaction = await instance.buyPack(packId, {value:price, from:acc});
    //console.log(transaction);
    //baseline gas:  3,652,876
    //after bitmask: 3,377,820
    //previous:      3,245,468

    let result  = await ownership.itemsOf(acc);
    let item1Id = result[0][0].toNumber();
    let item2Id = result[0][1].toNumber();

    assert.isTrue(item1Id >= 1 && item1Id <= itemsCount);
    assert.isTrue(item2Id >= 1 && item2Id <= itemsCount);

    let pack = await packManager.getPack(packId);
    assert.equal(pack[1].toNumber(),1,"minted did not match");
    assert.equal(pack[2].toNumber(),stock,"stock did not match");
  });

  //--------------------------------------------------------------------------
  it("should revert if pack is invalid", async function() {
    await assertRevert(
      instance.buyPack(100, {value:price})
    );
  });

  //--------------------------------------------------------------------------
  it("should revert if price is less", async function() {
    await assertRevert(
      instance.buyPack(packId, {value:price-100})
    );
  });

});
