var ItemManager = artifacts.require("./ItemManager.sol"),
    fs = require('fs'),
    rawData = fs.readFileSync('../data/data.json'),
    jsonData = JSON.parse(rawData),
    items = jsonData.items;

module.exports = function(callback) {

  var createItems = async function(ItemManager, count) {
    console.log("creating items: ", count);

    try {
      let result = await ItemManager.createItems(count);
      console.log("create items OK", result);
      return true;
    } catch (e) {
      console.log("create items Error", e);
      return false;
    }
  };

  // Create the items
  ItemManager.deployed().then(async(instance) => {

    //split into batches so we dont exceed the gas limit

    var batchSize = 10;
    var total = 0;

    while(total < items.length) {

      let c = batchSize;
      if (c > items.length - total)
        c = items.length - total;

      let result = await createItems(instance, c);
      if (result == false) {
        console.log("exiting populate");
        return;
      }
      total += c;
    }
    console.log("populate_1 done");
  });
};
