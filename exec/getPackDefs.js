var Ownership = artifacts.require("./Ownership.sol"),
    account = "0x627306090abab3a6e1400e9345bc60c78a8bef57";
    //account = "0x3233f67E541444DDbbf9391630D92D7F7Aaf508D"; // ropsten

module.exports = async function(callback) {

  try {
    let ownership = await Ownership.deployed();
    let result = await ownership.packDefsOf(account);
    let objs = [];
    let tokens = [];

    for (let i in result[0]) {
      objs.push(result[0][i].toNumber());
    }
    for (let i in result[1]) {
      tokens.push(result[1][i].toString());
    }
    console.log("PackDefs", objs, "\nTokens", tokens);

  } catch (e) {
    console.log("ERROR", e);
  }
};
