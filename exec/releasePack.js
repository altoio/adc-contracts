var PackManager = artifacts.require("./PackManager.sol");

module.exports = function (callback) {
  PackManager.deployed().then(async function(instance) {

    var releasePack = function(packManager, packId) {
      packManager.releasePack(packId).then(function(result) {
        console.log("releasePack OK", packId);
        return true;
      }).catch(function(e) {
        console.log("releasePack Error", e);
        return false;
      });
    };


    if (await releasePack(instance, 1) == false)
      return;

    if (await releasePack(instance, 2) == false)
      return;

    if (await releasePack(instance, 3) == false)
      return;
  });
};
