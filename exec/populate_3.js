var ItemManager = artifacts.require("./ItemManager.sol"),
    PackManager = artifacts.require("./PackManager.sol"),
    PackItems = artifacts.require("./PackItems.sol"),
    fs = require('fs'),
    rawData = fs.readFileSync('../data/data.json'),
    jsonData = JSON.parse(rawData),
    items = jsonData.items,
    packs = jsonData.packs;

module.exports = function(callback) {

  PackItems.deployed().then(async function(packItems) {

    //split into batches so we dont exceed the gas limit
    var len = items.length;
    var batchSize = 20;

    for (var p = 0; p < packs.length; p++) {

      var total = 0;
      var index = 1;

      while(total < len) {

        let c = batchSize;
        if (c > len - total)
          c = len - total;

        let items = [];
        let weights = [];

        for (var i=0; i<c; i++) {
          items[i] = index;
          weights[i] = 1;
          index++;
        }

        try {
          console.log("set pack items for pack", p+1);
          console.log(items, weights);
          let result = await packItems.setPackItems(p+1, items, weights);
          console.log(result);
        } catch (e) {
          console.log("set pack items Error", e);
          console.log("exiting populate");
        }

        total += c;
      }
    }
    console.log("populate_3 done");
  });
};
