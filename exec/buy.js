var ItemStore = artifacts.require("./ItemStore.sol");
var PackItems = artifacts.require("./PackItems.sol");

module.exports = async function(callback) {
  var packId = 1;

//  let pi = await PackItems.deployed();
//  let result = await pi.getPackItems(packId);
//  console.log(result[0]);
//  console.log(result[1]);
//  console.log(result[2]);


  ItemStore.deployed().then(function(instance) {
    instance.buyPack(packId, {value: 40000000000000000}).then(function(result) {
      console.log("BUYPACK SUCCESS\n", result);
    }).catch(function (e) {
      console.log("BUYPACK ERROR\n", e);
    });
  });
};
