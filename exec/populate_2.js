var PackManager = artifacts.require("./PackManager.sol"),
    fs = require('fs'),
    rawData = fs.readFileSync('../data/data.json'),
    jsonData = JSON.parse(rawData),
    packs = jsonData.packs;

module.exports = function(callback) {
  PackManager.deployed().then(async function(packManager) {
    console.log("creating packs");
    try {

      let sizes = [];
      let prices = [];
      let stocks = [];

      for (var i = 0; i < packs.length; i++) {
        sizes[i] = packs[i].size;
        prices[i] = packs[i].sellingPrice;
        stocks[i] = packs[i].stock;
      }

      let result = await packManager.createPacks( sizes, prices, stocks);
      console.log("createPacks OK", result);
      console.log("populate_2 done");

    } catch (e) {
      console.log("createPack Error", e);
      return false;
    }

  });
};
