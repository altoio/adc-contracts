var Ownership = artifacts.require("./Ownership.sol"),
    account = "0x627306090abab3a6e1400e9345bc60c78a8bef57";
    //account = "0x3233f67E541444DDbbf9391630D92D7F7Aaf508D"; // ropsten

module.exports = function(callback) {
  Ownership.deployed().then(async function(instance) {

    let tokens = [];
    let balance = await instance.balanceOf(account);
    for (let i=0; i<balance; i++) {
      let token = await instance.tokenOfOwnerByIndex(account, i);
      tokens.push(token);
    }

    console.log("GETTOKENCOUNT SUCCESS\n  ", tokens);
  });
};
