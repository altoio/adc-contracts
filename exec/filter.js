var ItemStore = artifacts.require("./ItemStore.sol");


module.exports = async function(callback) {

  //var account = "0x3233f67E541444DDbbf9391630D92D7F7Aaf508D"; // ropsten

  try {

    //var account = await web3.eth.accounts[0];
    var contract = await ItemStore.deployed();

    console.log(contract.address);

    var filter = web3.eth.filter({
      address:contract
    });

    // watch for changes
    filter.get(function(error, result){
      console.log(error, result);
    });


  } catch(e) {
    console.log(e);
  }

  console.log("Done");
};

