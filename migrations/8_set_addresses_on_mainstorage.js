var ItemManager  = artifacts.require("./ItemManager.sol"),
    PackManager  = artifacts.require("./PackManager.sol"),
    PackItems    = artifacts.require("./PackItems.sol"),
    MainStorage  = artifacts.require("./MainStorage.sol"),
    Ownership    = artifacts.require("./Ownership.sol"),
    ItemStore    = artifacts.require("./ItemStore.sol");

var coo = "0x1331B779A3D4Bd3f7E605e8c8A7D4Edcf692Fd6b";

module.exports = function(deployer) {
  deployer.then(async() => {
    // await deployer.deploy(MainStorage);
    // await deployer.deploy(ItemManager, MainStorage.address);
    // await deployer.deploy(PackManager, MainStorage.address);
    // await deployer.deploy(PackItems,   MainStorage.address);
    // await deployer.deploy(ItemStore,   MainStorage.address);
    // await deployer.deploy(Ownership,   "ADCLoot", "ADCL");

    let ms = await MainStorage.deployed();
    await ms.setContract("itemmanager", ItemManager.address);
    await ms.setContract("packmanager", PackManager.address);
    await ms.setContract("packitems",   PackItems.address);
    await ms.setContract("itemstore",   ItemStore.address);
    await ms.setContract("ownership",   Ownership.address);
    console.log("init main storage done");

    let ow = await Ownership.deployed();
    await ow.setAccess(ItemStore.address, true);
    await ow.setAccess(ItemManager.address, true);
    await ow.setAccess(PackManager.address, true);
    console.log("init ownership done");

    let pm = await PackManager.deployed();
    await pm.setAccess(ItemStore.address, true);
    await pm.setAccess(coo, true);
    console.log("init pack manager done");

    let pi = await PackItems.deployed();
    await pi.setAccess(ItemStore.address, true);
    await pi.setAccess(coo, true);
    console.log("init pack items done");

    let im = await ItemManager.deployed();
    await im.setAccess(ItemStore.address, true);
    await im.setAccess(coo, true);
    console.log("init item manager done");

    let is = await ItemStore.deployed();
    await is.setAccess(coo, true);
    console.log("init item store done");
  });
};
