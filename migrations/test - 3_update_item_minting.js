var ItemManager = artifacts.require("./ItemManager.sol"),
    PackManager = artifacts.require("./PackManager.sol"),
    PackItems   = artifacts.require("./PackItems.sol"),
    MainStorage = artifacts.require("./MainStorage.sol"),
    Ownership   = artifacts.require("./Ownership.sol"),
    ItemStore   = artifacts.require("./ItemStore.sol");

var coo = "0x1331B779A3D4Bd3f7E605e8c8A7D4Edcf692Fd6b";

module.exports = function(deployer) {
  deployer.then(async() => {

    let ms = await MainStorage.deployed();
    let it = await Ownership.deployed();
    let ps = await PackManager.deployed();
    let pi = await PackItems.deployed();
    let is = await ItemManager.deployed();

    await ms.deleteContract("itemstore",  ItemStore.address);
    await it.setAccess(ItemStore.address, false);
    await ps.setAccess(ItemStore.address, false);
    await pi.setAccess(ItemStore.address, false);
    await is.setAccess(ItemStore.address, false);
    console.log("deleted old item minting access " + ItemStore.address);

    await deployer.deploy(ItemStore,  MainStorage.address);

    console.log("setting new item minting access " + ItemStore.address);
    
    await ms.setContract("itemstore",  ItemStore.address);
    await it.setAccess(ItemStore.address, true);
    await ps.setAccess(ItemStore.address, true);
    await pi.setAccess(ItemStore.address, true); 
    await is.setAccess(ItemStore.address, true);

    let im = await ItemStore.deployed();
    await im.setAccess(coo, true);
    console.log("init item minting done");
  });
};

