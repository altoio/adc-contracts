var HDWalletProvider = require("truffle-hdwallet-provider");

var mnemonic = "";

require('babel-register')({
  ignore: /node_modules\/(?!openzeppelin-solidity\/test\/helpers)/
  //ignore: false
});
require('babel-polyfill');

try {
  var Mnemonic = require("./Mnemonic");
  var mnemonicInstance = new Mnemonic();
  mnemonic = mnemonicInstance.get();
} catch (error) { }

module.exports = {
  // See <http://truffleframework.com/docs/advanced/configuration>
  // to customize your Truffle configuration!
  networks: {
    local: {
      host: "localhost",
      port: 8545,
      network_id: "*"
    },
    ganache: {
      host: "localhost",
      port: 7545,
      network_id: "*"
    },
    ropsten: {
      provider: function() {
        return new HDWalletProvider(mnemonic, "https://ropsten.infura.io/GYq4NgMACMsJoZze9V1V")
      },
      network_id: 3,
      gas: 4612388,
      gasPrice: 4000000000 // gwei
      //gasPrice: 4000000000
    },
    rinkeby: {
      provider: function() {
        return new HDWalletProvider(mnemonic, "https://rinkeby.infura.io/GYq4NgMACMsJoZze9V1V")
      },
      network_id: 4,
      gas: 4612388,
      gasPrice: 4000000000
    },
    live: {
      provider: function() {
        return new HDWalletProvider(mnemonic, "https://mainnet.infura.io/GYq4NgMACMsJoZze9V1V")
      },
      network_id: 1,
      gas: 4612388,
      gasPrice: 4000000000 //4 gwei
      //gasPrice: 4000000000 //4 gwei
    },
  }
};
